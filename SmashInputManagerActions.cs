using Rewired;
public class SmashInputManagerActions : RewiredInputActions{
	public static string[] AllButtonActions = new string[] {"Jump","Target","Smash","Special","ToggleCamera","GetUp","Block","ToggleTargetLock","Jab","AirDodge","MoveForward","NextCharacter","NextTarget","PreviousTarget","UpAir","DownAir","AirAttack","Pause","A","B","X","Y","RB","LB","Start","Back","LeftStickClick","RightStickClick","LTButton","RTButton","LeftStickVertical","RightStickHorizontal","RightStickVertical","DPadHoriziontal","DPadVert"};
	public override string[] AllButtonNames {get {return AllButtonActions;}}
	public static string[] AllAxisActions = new string[] {"MovementHorizontal","MovementVertical","DpadHorizontal","DpadVertical","UpDownAxis","AltAxisHorizontal","AltAxisVertical","CameraHorizontal","CameraVertical","RTAxis","LTAxis","LeftStickHorizontal"};
	public override string[] AllAxisNames {get {return AllAxisActions;}}
	public static string[] NotAnimatorActions = new string[] {"ToggleCamera","NextTarget","PreviousTarget","CameraHorizontal","CameraVertical","Pause"};
	public override string[] NotAnimatorActionNames {get {return NotAnimatorActions;}}

	public static InputAction Jump {get{return Get("Jump", typeof(SmashInputManagerActions));}}
	public static InputAction Target {get{return Get("Target", typeof(SmashInputManagerActions));}}
	public static InputAction Smash {get{return Get("Smash", typeof(SmashInputManagerActions));}}
	public static InputAction Special {get{return Get("Special", typeof(SmashInputManagerActions));}}
	public static InputAction ToggleCamera {get{return Get("ToggleCamera", typeof(SmashInputManagerActions));}}
	public static InputAction MovementHorizontal {get{return Get("Movement Horizontal", typeof(SmashInputManagerActions));}}
	public static InputAction MovementVertical {get{return Get("Movement Vertical", typeof(SmashInputManagerActions));}}
	public static InputAction GetUp {get{return Get("GetUp", typeof(SmashInputManagerActions));}}
	public static InputAction Block {get{return Get("Block", typeof(SmashInputManagerActions));}}
	public static InputAction ToggleTargetLock {get{return Get("ToggleTargetLock", typeof(SmashInputManagerActions));}}
	public static InputAction DpadHorizontal {get{return Get("Dpad Horizontal", typeof(SmashInputManagerActions));}}
	public static InputAction DpadVertical {get{return Get("Dpad Vertical", typeof(SmashInputManagerActions));}}
	public static InputAction UpDownAxis {get{return Get("UpDownAxis", typeof(SmashInputManagerActions));}}
	public static InputAction Jab {get{return Get("Jab", typeof(SmashInputManagerActions));}}
	public static InputAction AltAxisHorizontal {get{return Get("AltAxis Horizontal", typeof(SmashInputManagerActions));}}
	public static InputAction AltAxisVertical {get{return Get("AltAxis Vertical", typeof(SmashInputManagerActions));}}
	public static InputAction AirDodge {get{return Get("AirDodge", typeof(SmashInputManagerActions));}}
	public static InputAction MoveForward {get{return Get("MoveForward", typeof(SmashInputManagerActions));}}
	public static InputAction NextCharacter {get{return Get("NextCharacter", typeof(SmashInputManagerActions));}}
	public static InputAction NextTarget {get{return Get("Next Target", typeof(SmashInputManagerActions));}}
	public static InputAction PreviousTarget {get{return Get("Previous Target", typeof(SmashInputManagerActions));}}
	public static InputAction UpAir {get{return Get("Up Air", typeof(SmashInputManagerActions));}}
	public static InputAction DownAir {get{return Get("Down Air", typeof(SmashInputManagerActions));}}
	public static InputAction AirAttack {get{return Get("Air Attack", typeof(SmashInputManagerActions));}}
	public static InputAction CameraHorizontal {get{return Get("Camera Horizontal", typeof(SmashInputManagerActions));}}
	public static InputAction CameraVertical {get{return Get("Camera Vertical", typeof(SmashInputManagerActions));}}
	public static InputAction Pause {get{return Get("Pause", typeof(SmashInputManagerActions));}}
	public static InputAction A {get{return Get("A", typeof(SmashInputManagerActions));}}
	public static InputAction B {get{return Get("B", typeof(SmashInputManagerActions));}}
	public static InputAction X {get{return Get("X", typeof(SmashInputManagerActions));}}
	public static InputAction Y {get{return Get("Y", typeof(SmashInputManagerActions));}}
	public static InputAction RB {get{return Get("RB", typeof(SmashInputManagerActions));}}
	public static InputAction RTAxis {get{return Get("RT Axis", typeof(SmashInputManagerActions));}}
	public static InputAction LB {get{return Get("LB", typeof(SmashInputManagerActions));}}
	public static InputAction LTAxis {get{return Get("LT Axis", typeof(SmashInputManagerActions));}}
	public static InputAction Start {get{return Get("Start", typeof(SmashInputManagerActions));}}
	public static InputAction Back {get{return Get("Back", typeof(SmashInputManagerActions));}}
	public static InputAction LeftStickClick {get{return Get("Left Stick Click", typeof(SmashInputManagerActions));}}
	public static InputAction RightStickClick {get{return Get("Right Stick Click", typeof(SmashInputManagerActions));}}
	public static InputAction LTButton {get{return Get("LT Button", typeof(SmashInputManagerActions));}}
	public static InputAction RTButton {get{return Get("RT Button", typeof(SmashInputManagerActions));}}
	public static InputAction LeftStickHorizontal {get{return Get("Left Stick Horizontal", typeof(SmashInputManagerActions));}}
	public static InputAction LeftStickVertical {get{return Get("Left Stick Vertical", typeof(SmashInputManagerActions));}}
	public static InputAction RightStickHorizontal {get{return Get("Right Stick Horizontal", typeof(SmashInputManagerActions));}}
	public static InputAction RightStickVertical {get{return Get("Right Stick Vertical", typeof(SmashInputManagerActions));}}
	public static InputAction DPadHoriziontal {get{return Get("DPad Horiziontal", typeof(SmashInputManagerActions));}}
	public static InputAction DPadVert {get{return Get("DPad Vert", typeof(SmashInputManagerActions));}}
}